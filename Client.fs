﻿namespace websharper_CRUD

open WebSharper
open WebSharper.UI
open WebSharper.UI.Templating
open WebSharper.UI.Notation
open WebSharper.UI.Html
open WebSharper.UI.Client

open System


[<JavaScript>]
module Templates =

    type MainTemplate = Templating.Template<"Main.html", ClientLoad.FromDocument, ServerLoad.WhenChanged>

[<JavaScript>]
module Client =

    // Helper
    let classes (l:string list) = attr.``class`` (String.concat " " l )

    let Main () =
      async {

        // Get the users from the server
        let! users = Server.GetUsers()
        // Create a ListModel, using the user's id as identifier.
        // We initialise the ListModel with an empty list
        // Notice this is totally independent from the retrieval of users from the server
        let usersCollection =
            ListModel.Create
                (fun (u:DataStore.User) -> u.id)
                []
        // A Var holding some feedback to the user
        let resultVar = Var.Create Doc.Empty
        // If the editedUser Var holds Some user, we will display the edition for for that user
        let editedUser:Var<Option<DataStore.UserSpec>> = Var.Create None
        //
        // Handle the users retrieved from the server
        // and update the list model accordingly.
        match users with
        // If we successful, update the ListModel
        | Ok l ->
            usersCollection.Set l
        // In case of error, empty the ListModel and notify user
        | Error es ->
            resultVar.Set(
              div [] [text "Error retrieving users"]
            )


        // We map the ListModel to a Doc, with using cached data for unmodified elements
        // Note the argument to the function is an individual item, and not the whole sequence
        let usersRows =
          usersCollection.View.DocSeqCached (fun (u:DataStore.User) ->
                                    tr
                                        []
                                        [
                                            td [attr.``class`` "table-sm"] [text u.name]
                                            td [attr.``class`` "table-sm"; on.click (fun _el _ev -> editedUser.Set (u.toSpec()|> Some))] [a [attr.href "#"] [text "Edit"]]
                                        ]
                                  )
        // Map the ListModel to a table.
        let usersTableView =
                table
                    [ attr.``class`` "table table-bordered table-striped table-hover table-sm"; attr.id "admin_users_table" ]
                    [
                        thead []
                              [
                                tr []
                                   [
                                        th []
                                           [
                                                div []  [text "Name"]
                                           ]
                                        th []
                                           [
                                                div []  [text "Edit"]
                                           ]

                                   ]

                                ]

                        tbody []
                              [usersRows]

                    ]

        // Title of the form indicating if we do a user creation or edition
        let formTitleView =
          editedUser.View
          |> View.Map (fun userSpecOption ->
            userSpecOption
            |> Option.map (fun userSpec ->
              match userSpec.id with
              | Some _ -> "Edit user"
              | None -> "Create user"
            )
            // If editedUser is None, we don't care as the form is not shown, but we set the title to ""
            |> Option.defaultValue ""
          )
        // This function returns the user form.
        // It handles both edit and creations, depending of the value in the id field (resp Some and None)
        let editionFormForUser (user:DataStore.UserSpec) =
            // Put the user in a var and create lenses to easily link it to form fields
            let u = Var.Create user
            // Create a lens on the name, giving us a Var we can use in the form field
            let name =
                u.LensAuto (fun u -> u.name)
            // Return the form Doc
            form
                [ attr.id "admin_users_form"]
                [
                    // To keep the diff small, I add the title in here.
                    b [] [text formTitleView.V]
                    br [] []
                    // The name field and its label
                    label [attr.``for`` "userEditionName"] [text "Name"]
                    Doc.InputType.Text [attr.id "userEditionName"] name
                    // Submid button
                    Doc.Button
                        "Save"
                        [attr.``class`` "btn btn-primary"]
                        // This is handling the click
                        (fun () ->
                           async {
                                    // Call the server side
                                    let! r =Server.addOrEditUser u.Value
                                    match r with
                                    | Ok userFromServer ->
                                        // We get the updated user back, inject it in the users collection used to display the table
                                        // Add updates an existing entry
                                        usersCollection.Add userFromServer
                                        // Notify of the success
                                        resultVar.Set (div [] [text "UpdateSuccessful"])
                                        // Get out of user edition, hiding the form
                                        editedUser.Set None
                                    // This Ok l should not happen as single row  result is ensured server side
                                    | _ ->
                                      resultVar.Set (div [] [text "An error occured"])
                            }
                            |> Async.Start
                        )
                    Doc.Button
                        "Cancel"
                        [attr.``class`` "btn btn-secondary"]
                        // Cancelling is simply going to a state where no user edition is done
                        (fun () -> editedUser.Set None)
                ]
        // The view displaying the for or an empty doc according to the editedUser Var value.
        // We place this view in the doc we return below
        let editionformView  =
            editedUser.View
            |> View.Map (fun uo ->
                match uo with
                | None -> Doc.Empty
                | Some u -> editionFormForUser u
            )

        // View giving the label of the addition button.
        let addButtonTextView =
          editedUser.View
          |> View.Map (function |Some _ -> "Cancel" | None -> "Add new")
        // Button to add or edit a user
        let addButtonView =
          addButtonTextView
          |> View.Map (fun addButtonText ->
            Doc.Button
                addButtonText
                [attr.``class`` "btn btn-primary"]
                (fun () ->
                    resultVar.Set Doc.Empty
                    // Set the value in editedUser to an "empty" record with
                    // its id field set to None indicating it is not yet stored in the database
                    editedUser.Set (Some(DataStore.UserSpec.Init())))
          )

        // Finally, return the Doc, inserting the view of the table
        return
          div
            []
            [
              resultVar.V
              addButtonView.V
              editionformView.V
              usersTableView
            ]

      }
      // Map our Async<Doc> to a Doc
      |>Client.Doc.Async
