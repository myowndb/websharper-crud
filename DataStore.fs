namespace websharper_CRUD
open WebSharper

module DataStore=
  [<JavaScript>]
  type UserSpec =
    {id: int option; name:string}
    with
    static member Init() =
      { id= None; name=""}
  [<JavaScript>]
  type User = { id: int; name:string}
    with
    member self.toSpec() : UserSpec =
      { id= Some (self.id); name = self.name}


  type Store() =
    let mutable nextId = 4
    let mutable store:User array = [| {id=1; name="Jon"}; {id=2; name="Jane"}; {id=3;name="Jim"}|]
    with
    member _.updateStore (userSpec:UserSpec) =
      match userSpec.id with
      | Some existingId ->
        // Build user record from spec. This will probably be the data returned from
        // the database after the update, like postgres does with "RETURNING *"
        let userFromSpec = {id= existingId;name=userSpec.name}
        store <-
          (store
          |> Array.map
            (fun u ->
              if u.id <> existingId then
                u
              else
                {u with name=userSpec.name}
            )
          )
        // Return the user record from the database
        userFromSpec

      | None ->
        // Build user record from spec. This will probably be the data returned from
        // the database after the insert, like postgres does with "RETURNING *"
        let userFromSpec = {id=nextId; name = userSpec.name }
        store <-
          (Array.append
            store
            [| userFromSpec|]
          )
        nextId <- nextId + 1
        // Return the user record from the database
        userFromSpec

    member _.read() = store
