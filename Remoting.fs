﻿namespace websharper_CRUD

open WebSharper

module Server =

    [<Rpc>]
    let DoSomething input =
        let R (s: string) = System.String(Array.rev(s.ToCharArray()))
        async {
            return R input
        }

    [<Rpc>]
    let GetUsers () : Async<Result<DataStore.User array,string>>=
        let ctx = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let dataStore:DataStore.Store = unbox (serviceProvider.GetService(typeof<DataStore.Store>))
        async {
            return Ok (dataStore.read())
        }

    [<Rpc>]
    let addOrEditUser (userSpec:DataStore.UserSpec) : Async<Result<DataStore.User,string>>=
        let ctx = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let dataStore:DataStore.Store = unbox (serviceProvider.GetService(typeof<DataStore.Store>))
        async {
            return Ok (dataStore.updateStore(userSpec))
        }
